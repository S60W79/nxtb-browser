/*
 * Api endpoints as async functions.
 * The responsibility of the occuring data lies at nextbike or Tier Mobility SE
 * The Api Key has been reverse engineered, there are several keys available.
*/
var apik = "rXXqTgQZUPZ89lzB";
var apikey = "rXXqTgQZUPZ89lzB";
var servertime = 0;

function isJsonString(str) {
    //helper to check if answer is correct
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
function bikeState(authkey, nr){
//shows user details about the bike without logging in.
return new Promise((resolve, reject) => {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/getBikeState.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
    if (xhr.status == 200) {

        var plain =  xhr.responseText;
        if (plain != null && plain != "" && isJsonString(plain)){
            var sheet = JSON.parse(plain);
                document.getElementById('offerVsDemand').innerHTML = sheet["bike"]["state_description"];
                sheet["bike"]["texts"].forEach(function(txt){
                if(txt.type =="biketype_name" || txt.type == "biketype_description" || txt.nodeValue == "")return;
                document.getElementById('offerVsDemand').innerHTML += '<br><div class="entry-label critic">'+txt.nodeValue+'</div>'
                
                });
                
                document.getElementById('offerVsDemand').style.display = "block";
                document.getElementById("placeIdSide").innerHTML = sheet["bike"]["place_id"];
                document.getElementById('advancedSide').innerHTML = "<br>"+createTableFromJson(sheet["bike"]);

            resolve(sheet["bike"]["place_id"]);
        }
    }else{
    reject();

    }
    };

    var data = `{
    "apikey": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "bike": "`+nr+`",
    "show_errors":1 
    }`;

    xhr.send(data); 
});
} 



function breakBike(apik, bikenr, authkey, end){
    //Activate Break mode (bike will stay rent when lock closed by the user)
    return new Promise((resolve, reject) => {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://api.nextbike.net/api/rentalBreak.json");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            if (xhr.status == 200) {
                var plain =  xhr.responseText;
                if (plain != null && plain != ""){
                    var sheet = JSON.parse(plain);
                        resolve(sheet);

                }
            }else{
                reject(xhr.status);
            }
        };

        var data = `{
        "apikey": "`+apikey+`",
        "bike": `+parseInt(bikenr)+`,
        "loginkey": "`+authkey+`",
        "show_errors":1 
        }`;
        xhr.send(data); 
	});
}

function open(apik, bikenr, authkey){
    //open lock of a bike rent by the user, if the lock is closed (due to parking mode or a rental without unlock)
    return new Promise((resolve, reject) => {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://api.nextbike.net/api/openLock.json");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            if (xhr.status == 200) {
                var plain =  xhr.responseText;
                if (plain != null && plain != ""){
                    var sheet = JSON.parse(plain);
                        resolve(sheet);

                }
            }else{
                    reject(xhr.status);
            }
        };

        var data = `{
        "apikey": "`+apikey+`",
        "bike": `+parseInt(bikenr)+`,
        "loginkey": "`+authkey+`",
        "show_errors":1 
        }`;
        xhr.send(data); 
	});
}
async function retbike(legacy, authkey, bikenr, advanced){
//return the bike
return new Promise((resolve, reject) => {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/return.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (xhr.status == 200) {
            console.log(xhr.status);
            var plain =  xhr.responseText;
            if (plain != null && plain != ""){
                var sheet = JSON.parse(plain);
                    resolve(sheet);

            }
        }else{
            console.log("error", xhr.status);
            console.log("error", xhr.responseText);
                reject(xhr.status);

        }

    };

    var data = `{
    "api_key": "`+apikey+`",
    "bike": "`+bikenr+`",
    "loginkey": "`+authkey+`",
    "show_errors":1`+advanced+`
    }`;
    xhr.send(data); 
});
}

async function update(legacy, authkey, id, advanced){
//return the bike
return new Promise((resolve, reject) => {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/updateRental.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (xhr.status == 200) {
            console.log(xhr.status);
            var plain =  xhr.responseText;
            console.log("RET answer:", plain);
            if (plain != null && plain != ""){
                var sheet = JSON.parse(plain);
                    resolve(sheet);

            }
        }else{
            console.log("error", xhr.status);
            console.log("error", xhr.responseText);
                reject(xhr.status);

        }

    };

    var data = `{
    "api_key": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "rental": `+id+`,
    "show_errors":1`+advanced+`
    }`;
    console.log(data);
    xhr.send(data); 
});
}

async function cancel(legacy, authkey, id){
//return the bike
return new Promise((resolve, reject) => {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/cancelBooking.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (xhr.status == 200) {
        console.log(xhr.status);
        var plain =  xhr.responseText;
        console.log("RET answer:", plain);
        if (plain != null && plain != ""){
            var sheet = JSON.parse(plain);
                resolve(sheet);

        }
        }else{
            console.log("error", xhr.status);
            console.log("error", xhr.responseText);
                reject(xhr.status);

        }

    };

    var data = `{
    "api_key": "`+apikey+`",
    "booking_id": `+id+`,
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    console.log(data);
    xhr.send(data); 
});
}

async  function placeInf(legacy, authkey, place){
	return new Promise((resolve, reject) => {
        //rent a bike

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://api.nextbike.net/api/getPlaceDetails.json");
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            if (xhr.status == 200) {
                console.log(xhr.status);
                var plain =  xhr.responseText;
                console.log("Rentals...", plain);
                
                if (plain != null && plain != ""){
                        var sheet = JSON.parse(plain)["place"];
                        console.log("here...", sheet);

                            resolve(sheet);
                }
            }else{
                //console.log("error", xhr.status);
                //console.log("errorcurr...", xhr.responseText);
                    reject([]);
            }
        };
console.log("APK", apikey);
        var data = `{
        "apikey": "`+apikey+`",
        "loginkey": "`+authkey+`",
        "place": "`+place+`",
        "show_errors":1 
        }`;
        //console.log(data);
        xhr.send(data); 
    });
}

async  function current(legacy, authkey){
//give out a list of all current rentals of the user
return new Promise((resolve, reject) => {
    //rent a bike

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/getOpenRentals.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
    if (xhr.status == 200) {
        console.log(xhr.status);
        var plain =  xhr.responseText;
        console.log("Rentals...", plain);
        
        if (plain != null && plain != ""){
				var plainJS = JSON.parse(plain)
                var sheet = plainJS["rentalCollection"];
                    document.getElementById('indicLst').innerHTML = sheet.length;
					servertime = plainJS["server_time"]; //store Servertime for calculation of length

                    resolve(sheet);
        }
    }else{
        //console.log("error", xhr.status);
        //console.log("errorcurr...", xhr.responseText);
            reject([]);
    }
    };

    var data = `{
    "apikey": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    //console.log(data);
    xhr.send(data); 
});
}
async  function history(amt, authkey){
//give out a list of all current rentals of the user
return new Promise((resolve, reject) => {
    //rent a bike

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/list.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
    if (xhr.status == 200) {
        console.log(xhr.status);
        var plain =  xhr.responseText;
        
        if (plain != null && plain != ""){
                

                    resolve(plain);
        }
    }else{
        //console.log("error", xhr.status);
        //console.log("errorcurr...", xhr.responseText);
            reject([]);
    }
    };

    var data = `{
    "apikey": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "limit":`+amt+`,
    "show_errors":1 
    }`;
    //console.log(data);
    xhr.send(data); 
});
}

async  function bookings(legacy, authkey){
return new Promise((resolve, reject) => {
    //rent a bike

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/bookings.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (xhr.status == 200) {
            console.log(xhr.status);
            var plain =  xhr.responseText;
            console.log("Rentals...", plain);
            
            if (plain != null && plain != ""){
                var sheet = JSON.parse(plain)["items"];
                    //document.getElementById('indicLst').innerHTML = sheet.length;

                    resolve(sheet);
            }
        }else{
            //console.log("error", xhr.status);
            //console.log("errorcurr...", xhr.responseText);
                console("!!!", xhr.responseText)
                reject([]);
        }
    };

    var data = `{
    "apikey": "`+apikey+`",
    "loginkey": "`+authkey+`",
    "show_errors":1 
    }`;
    //console.log(data);
    xhr.send(data); 
});
}

async  function lstCities(){
//give out a list of countries (in which are a list of cities)
return new Promise((resolve, reject) => {
    //rent a bike

    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://maps.nextbike.net/maps/nextbike-official.json?list_cities=1&include_domains=de,ue,ug,um,ur,bh");
    // xhr.setRequestHeader("Accept", "application/json");
    // xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
        if (xhr.status == 200) {
            console.log(xhr.status);
            var plain =  xhr.responseText;
            console.log("Rentals...", plain);
            
            if (plain != null && plain != ""){
                var sheet = JSON.parse(plain)["countries"];
                    //document.getElementById('indicLst').innerHTML = sheet.length;

                    resolve(sheet);
            }
        }else{
        //console.log("error", xhr.status);
        //console.log("errorcurr...", xhr.responseText);
            reject([]);
        }
    };

    xhr.send(null); 
});
}

async  function book(legacy, authkey, place, start, end, amt, type){
    console.log(end)
//Book bike(s) on a defined place (single bikes are also booked, defining the place ID of the specific bike)
return new Promise((resolve, reject) => {
    //rent a bike

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://api.nextbike.net/api/booking.json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
    if (xhr.status == 200) {
        console.log(xhr.status);
        var plain =  xhr.responseText;
        console.log("Rentals...", plain);
        
        if (plain != null && plain != ""){
            var sheet = JSON.parse(plain)["place"];

            resolve(sheet);
        }
    }else{
        //console.log("error", xhr.status);
        //console.log("errorcurr...", xhr.responseText);
            reject([]);
            console.log("!!!", xhr.responseText);
    }
    };
	if (start == 0){
		//start = now -> ignore start_time
        console.log(end)
        if(type == "0"){
            //if no type set: use various
            data = `{
            "apikey": "`+apikey+`",
            "loginkey": "`+authkey+`",
            "place": "`+place+`",
            "end_time": `+((end != end)?'0':end)+`,
            "num_bikes": `+amt+`,
            "show_errors":1 
            }`;
        }else{
            //use type!
            data = `{
            "apikey": "`+apikey+`",
            "loginkey": "`+authkey+`",
            "place": "`+place+`",
            "end_time": `+((end != end)?'0':end)+`,
            "biketypes": {"`+type+`":`+amt+`},
            "show_errors":1 
            }`;
        }
	}else{
		//start_time set by user
		var data = `{
		"apikey": "`+apikey+`",
		"loginkey": "`+authkey+`",
		"place": "`+place+`",
		"start_time": `+start+`,
		"end_time": `+((end != end)?'0':end)+`,
		"num_bikes": `+amt+`,
		"show_errors":1 
		}`;
	}
	console.log("book data:", data);
    //console.log(data);
    xhr.send(data); 
});
}
async function userDetails(authkey){
    console.log("APK",apikey)
			//shows user details
    return new Promise((resolve, reject) => {
		console.log("hi")

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "https://api.nextbike.net/api/getUserDetails.json");
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onload = function () {
			console.log("hi", xhr.status)
		if (xhr.status == 200) {

		var plain =  xhr.responseText;
		console.log("\nplain:",plain,"\n");
		if (plain != null && plain != "" && isJsonString(plain)){
			var sheet = JSON.parse(plain);

                console.log("back", sheet["user"]);
				resolve(sheet["user"]);
		}
		}else{

			reject(xhr.responseText);
		}
		};

		var data = `{
		"apikey": "`+apikey+`",
		"loginkey": "`+authkey+`",
		"show_errors":1 
		}`;

		xhr.send(data); 
    });
}



async function readRfid(authkey){
			//shows user details
    return new Promise((resolve, reject) => {
		console.log("hi")

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "https://api.nextbike.net/api/getCustomerRfids.json");
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onload = function () {
			console.log("hi", xhr.status)
		if (xhr.status == 200) {

		var plain =  xhr.responseText;
		console.log("\nplain:",plain,"\n");
		if (plain != null && plain != "" && isJsonString(plain)){
			var sheet = JSON.parse(plain);

                console.log("back", sheet);
				resolve(sheet);
				
		}
		}else{
			reject(xhr.responseText);

		}
		};

		var data = `{
		"apikey": "`+apikey+`",
		"loginkey": "`+authkey+`",
		"show_errors":1 
		}`;

		xhr.send(data); 
    });
}

async function setRfid(authkey, uid, nr1, nr2){
			//shows user details
    return new Promise((resolve, reject) => {
		console.log("hi")

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "https://api.nextbike.net/api/setCustomerRfid.json");
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onload = function () {
			console.log("hi", xhr.status)
		if (xhr.status == 200) {

		var plain =  xhr.responseText;
		console.log("\nplain:",plain,"\n");
		if (plain != null && plain != "" && isJsonString(plain)){
			var sheet = JSON.parse(plain);

                console.log("back", sheet);
				resolve(sheet);
		}
		}else{

			reject(xhr.responseText);
		}
		};

		var data = `{
		"apikey": "`+apikey+`",
		"loginkey": "`+authkey+`",
        "rfid": `+parseInt(uid)+`,
        "uid":14872084,
        "uid": `+parseInt(uid)+`,
        "rfid_uid": `+parseInt(uid)+`,
        "rfid-uid": `+parseInt(uid)+`,
        "type": "girocard",
        "expiry_date": 1694954785,
        "card_number1": "`+nr1+`",
        "card_number2": "`+nr2+`",
		"show_errors":1 
		}`;

		xhr.send(data); 
    });
}

async function unLog(authkey, scope="all"){
			//Log out (making the given authkey invalid)
    return new Promise((resolve, reject) => {
		console.log("hi")

		var xhr = new XMLHttpRequest();
		xhr.open("POST", "https://api.nextbike.net/api/logout.json");
		xhr.setRequestHeader("Accept", "application/json");
		xhr.setRequestHeader("Content-Type", "application/json");
		xhr.onload = function () {
			console.log("hi", xhr.status)
		if (xhr.status == 200) {

		var plain =  xhr.responseText;
		console.log("\nplain:",plain,"\n");
		if (plain != null && plain != "" && isJsonString(plain)){
			var sheet = JSON.parse(plain);

                console.log("back", sheet);
				resolve(sheet);
		}
		}else{

			reject(xhr.responseText);
		}
		};

		var data = `{
		"apikey": "`+apikey+`",
		"loginkey": "`+authkey+`",
        "scope": "`+scope+`",
		"show_errors":1 
		}`;

		xhr.send(data); 
    });
}

async function rentSingle(number, paused, resume, nresume){
        return new Promise((resolve, reject) => {

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://api.nextbike.net/api/rent.json");
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onload = function () {
		console.log("rental", xhr.responseText);
			if (xhr.status == 200) {
			var plain =  xhr.responseText;
			var sheet = JSON.parse(plain);
			if (plain != null && plain != ""){

                    resolve(sheet);
			}
			}else{
				reject();
			}
			};

			var data = `{
			"apikey": "`+apikey+`",
			"bike": `+number+`,
			"loginkey": "`+authkey+`",
			"start_paused":`+paused+`,
			"resume":`+resume+`,
			"no_resume":`+nresume+`,
			"show_errors":1
			}`;
			
			xhr.send(data); 
        });
}


async function typeGroups(){
        return new Promise((resolve, reject) => {

			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://api.nextbike.net/api/getBikeTypeGroups.json");
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
			xhr.onload = function () {
		console.log("rental", xhr.responseText);
			if (xhr.status == 200) {
			var plain =  xhr.responseText;
			var sheet = JSON.parse(plain);
			if (plain != null && plain != ""){

                    resolve(sheet);
			}
			}else{
				reject();
			}
			};

			var data = `{
			"apikey": "`+apikey+`",
			"show_errors":1
			}`;

			xhr.send(data);
        });
}


async function types(state){

        return new Promise((resolve, reject) => {
const date = new Date(state * 1000);

        // Format the date to an HTTP-date string
        const httpDate = date.toUTCString();
			var xhr = new XMLHttpRequest();
			xhr.open("POST", "https://bikes.dvb.solutions/envelopes/allTypes.php");
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader('If-Modified-Since', httpDate);
			xhr.onload = function () {
		console.log("rental", xhr.responseText);
			if (xhr.status == 200) {
			var plain =  xhr.responseText;
			var sheet = JSON.parse(plain);
			if (plain != null && plain != ""){

                    resolve(sheet);
			}
			}else{
				reject();
			}
			};

			var data = `{
			"apikey": "`+apikey+`",
			"show_errors":1
			}`;

			xhr.send(data);
        });
}


async function updateEnv(state,url){
//update generic envelopes
        return new Promise((resolve, reject) => {
const date = new Date(state * 1000);

        // Format the date to an HTTP-date string
        const httpDate = date.toUTCString();
			var xhr = new XMLHttpRequest();
			xhr.open("POST", url);
			xhr.setRequestHeader("Accept", "application/json");
			xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader('If-Modified-Since', httpDate);
			xhr.onload = function () {
		console.log("rental", xhr.responseText);
			if (xhr.status == 200) {
			var plain =  xhr.responseText;
			var sheet = JSON.parse(plain);
			if (plain != null && plain != ""){

                    resolve(sheet);
			}
			}else{
				reject();
			}
			};
			xhr.send({});
        });
}
