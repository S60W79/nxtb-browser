<?php
// Set the base API endpoint
$apiUrl = 'https://api.nextbike.net/api/infoFeed.json?api_key=rXXqTgQZUPZ89lzB&loginkey=iSA8tqMJAV8qw3Lp&hours=0'.$_SERVER['QUERY_STRING']; // Replace with your actual API URL
$opts = [
    "http" => [
        "method" => "GET",
        "header" => "Accept-Language: en-US,en;q=0.5"
    ]
];

// DOCS: https://www.php.net/manual/en/function.stream-context-create.php


header('Content-Type: application/rss+xml; charset=utf-8');

// Start the RSS feed
echo '<?xml version="1.0" encoding="UTF-8"?> ';
echo '<rss version="2.0">';
echo '<channel>';
echo '<title>Nextbike Service</title>';
echo '<link>https://bikes.dvb.solutions</link>';
echo '<description>Nextbike individual service feed. Warning, all get parameters are proxied by this php backend.</description>'; // Replace with your feed description
$expire = intval($_GET["expire"]);
if($expire-time() < 604800 && isset($_GET["expire"])){
    //key will expire in ~ a week. Inform about
    echo '<item>';
    echo '<title>Loginkey expires!</title>';
    echo '<description>Your key, able to obtain the RSS news, will expire in one week or less. Please regenerate your RSS link to further obtain messages. You can use the link in this RSS message to create a new Feed URL.</description>';
    echo '<pubDate>' . date(DATE_RSS, $expire-604800) . '</pubDate>';
    echo '<guid>' . strval($expire) . '</guid>';
    echo '<link>/rss/</link>';
    echo '</item>';
    if($expire-time() < -300){
        //defenetly expired. Break to reduce nxtb errors
        echo '</channel>';
        echo '</rss>';
        die();
    }
}
$apiUrl = str_replace("expire=".strval($expire)."&", "", $apiUrl);
$apiUrl = str_replace("&expire=".strval($expire), "", $apiUrl);
if(isset($_GET["language"])){
    // normal request
    $data = file_get_contents($apiUrl, true);
}else{
    //if Language not set, set header to english.
    $context = stream_context_create($opts);
    $data = file_get_contents($apiUrl, true, $context);
}

$data = json_decode($data, true);
// Loop through each news item and create an RSS item
foreach ($data['infoitems'] as $item) {
    echo '<item>';
    echo '<title>' . htmlspecialchars($item['title']) . '</title>';
    echo '<description>' . (array_key_exists("description",$item)?htmlspecialchars($item['description']):""). (array_key_exists("text",$item)?htmlspecialchars($item['text']):"") . (($item['node'] == 'voucher' && array_key_exists('code', $item))?(" code: ".$item["code"]):"").'</description>';
    echo '<pubDate>' . date(DATE_RSS, $item['tstamp']) . '</pubDate>';
    echo '<guid>' . htmlspecialchars($item['id']) . '</guid>';
    echo '</item>';
}
//
// End the RSS feed
echo '</channel>';
echo '</rss>';
//}
?>
