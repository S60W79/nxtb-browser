<?php
// Set the base API endpoint


header('Content-Type: application/rss+xml; charset=utf-8');

// Start the RSS feed
function init(){
    echo '<?xml version="1.0" encoding="UTF-8"?> ';
    echo '<rss version="2.0">';
    echo '<channel>';
    echo '<title>Nextbike NEWS</title>';
    echo '<link>https://bikes.dvb.solutions</link>';
    echo '<description>Nextbike news feed. Warning, all get parameters are proxied by this php backend.</description>';
}
$expire = intval($_GET["expire"]);
if($expire-time() > -300 || !(isset($_GET["expire"]))){
    //not expired or state unknown -> load data from nxtb

    $apiUrl = 'https://api.nextbike.net/api/getNews.json?'.$_SERVER['QUERY_STRING']; // Replace with your actual API URL
    // remove get parameters which are to steer the php. Don't proxy them to nxtb
    $apiUrl = str_replace("expire=".strval($expire)."&", "", $apiUrl);
    $apiUrl = str_replace("&expire=".strval($expire), "", $apiUrl);

    //get data from nxtb
    $data = file_get_contents($apiUrl);
    $data = json_decode($data, true);


    // Check if the Last-Modified header exists
    if (isset($_SERVER['HTTP_LAST_MODIFIED'])) {
        // Get the Last-Modified header value
        $lastModified = $_SERVER['HTTP_LAST_MODIFIED'];

        // Convert the Last-Modified date to a Unix timestamp
        $timestamp = (strtotime($lastModified));

        if(array_key_exists("news_collection", $data)){
            //echo($data["news_collection"][0]["created_time"]<=$timestamp)?"OK":"-";
            $missedMessage = (($data["news_collection"][0]["created_time"])>$timestamp);
            $missedNotice=($expire-604800<=$timestamp && $expire-time() < 604800);
            if(!($missedMessage) && !($missedNotice)){
                //neither the last recent post nor the expire notification have been created after the last request from the client -> code not modified
                Header("HTTP/1.1 304 Not Modified");
                exit();
            }
        }else{
            echo json_decode($data);
        }
    }

}
init();

$anounced = 0;
if($expire-time() < 604800 && isset($_GET["expire"])){
    $anounced = $expire-604800;
    //key will expire in ~ a week. Inform about
    echo '<item>';
    echo '<title>Loginkey expires!</title>';
    echo '<description>Your key, able to obtain the RSS news, will expire in one week or less. Please regenerate your RSS link to further obtain messages. You can use the link in this RSS message to create a new Feed URL.</description>';
    echo '<pubDate>' . date(DATE_RSS, $anounced) . '</pubDate>';
    echo '<guid>' . strval($expire) . '</guid>';
    echo '<link>/rss/</link>';
    echo '</item>';
    if($expire-time() < -300){
        //defenetly expired. Break to reduce nxtb errors
        echo '</channel>';
        echo '</rss>';
        die();
    }

}

// Loop through each news item and create an RSS item
foreach ($data['news_collection'] as $item) {
    echo '<item>';
    echo '<title>' . htmlspecialchars($item['title']) . '</title>';
    echo '<link>' . htmlspecialchars($item['url_webview']) . '</link>';
    echo '<description>' . htmlspecialchars($item['teaser_text']) . '</description>';
    echo '<pubDate>' . date(DATE_RSS, $item['created_time']) . '</pubDate>';
    echo '<guid>' . htmlspecialchars($item['uid']) . '</guid>';
    if (!empty($item['featured_image'])) {
        echo '<image><url>'.$item['featured_image'].'</url></image>';
    }
    echo '</item>';

}
//
// End the RSS feed
echo '</channel>';
echo '</rss>';
//}
?>
