async function anounce(){
//return the bike
return new Promise((resolve, reject) => {

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "https://bikes.dvb.solutions/special/announcment.html");
    xhr.setRequestHeader("Accept", "text/html");
    xhr.setRequestHeader("Content-Type", "text/html");
    xhr.onload = function () {
        if (xhr.status == 200) {
            let an =  xhr.responseText;
            //abuse prevention (no non GNU GPL javascript possible, only plain html/text)
            an = "<noscript>"+an.replace("</noscript>", "")+"</noscript><br><a href='https://bikes.dvb.solutions/special/announcment.html>Full announcement</a>"
            resolve(an);
        }else{
            console.log("error", xhr.status);
            console.log("error", xhr.responseText);
                reject(xhr.status);

        }

    };
    xhr.send();
});
}
