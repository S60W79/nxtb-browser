<?php
/*
 * Script which caches information from nextbike
 * Loadbalancing and validating if caches on the clients are up-to-date or information has to be sent.
 * GNU-GPL V3 by S60W79
 */

// Handle preflight requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, If-Modified-Since');
    header('Access-Control-Max-Age: 86400'); // Cache for 1 day
    exit;
}

$cacheDir = 'cache/allTypes/';
$cacheTime = 5000; // renew cache every 5000 sec


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
 header('Access-Control-Allow-Headers: Content-Type, Authorization, If-Modified-Since');
header('Content-Type: application/json');
function getApiResponse($url) {
    global $cacheDir, $cacheTime;

    $cacheKey = md5($url);
    $cacheFile = $cacheDir . $cacheKey . '.json';

    if (file_exists($cacheFile)) {
        $fileContent = file_get_contents($cacheFile);
        $cachedData = json_decode($fileContent, true);

        if ($cachedData && isset($cachedData['server_time'])) {
            $originTime = $cachedData['server_time'];
            $fileTime = filemtime($cacheFile);

            // If cache is still valid based on cacheTime
            if (time() - $fileTime < $cacheTime) {
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $originTime) . ' GMT');
                if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $originTime) {
                    header('HTTP/1.1 304 Not Modified');
                    exit;
                }
                return $fileContent;
            } else {
                // Check if client's content is still valid
                if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $originTime) {
                    header('HTTP/1.1 304 Not Modified');
                    exit;
                }
            }
        }
    }

    // Fetch from API if cache is not valid or outdated
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response !== false) {
        $data = json_decode($response, true);
        if (json_last_error() === JSON_ERROR_NONE) {
            if (isset($data['server_time'])) {
                $originTime = $data['server_time'];
                file_put_contents($cacheFile, $response);
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $originTime) . ' GMT');
            } else {
                // Handle the case where 'time' is not set in the response
                header('HTTP/1.1 500 Internal Server Error');
                echo json_encode(['error' => 'Invalid response structure, "server_time" field missing']);
                exit;
            }
        } else {
            // Handle JSON decode error
            header('HTTP/1.1 500 Internal Server Error');
            echo json_encode(['error' => 'Failed to decode JSON response']);
            exit;
        }
    }

    return $response;
}

// Usage
$apiUrl = 'https://api.nextbike.net/api/getBikeTypes.json?api_key=rXXqTgQZUPZ89lzB'; // Replace with the actual API endpoint
$response = getApiResponse($apiUrl);

echo $response;
?>
