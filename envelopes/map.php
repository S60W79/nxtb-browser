<?php
/*
 * Script which caches information from nextbike
 * Loadbalancing and validating if caches on the clients are up-to-date or information has to be sent.
 * GNU-GPL V3 by S60W79
 */
$cacheDir = 'cache/map/';
$cacheTime = 20; // renew cache every 20 sec

function getApiResponse($city) {
    global $cacheDir, $cacheTime;

    $cacheKey = md5($city);
    $cacheFile = $cacheDir . $cacheKey . '.json';

    if (file_exists($cacheFile)) {
        $fileTime = filemtime($cacheFile);

        if (time() - $fileTime < $cacheTime) {
            // Cache is valid
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $fileTime) . ' GMT');
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $fileTime) {
                header('HTTP/1.1 304 Not Modified');
                exit;
            }
            return file_get_contents($cacheFile);
        } else {
            // Cache is outdated, check if client's content is still valid
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $fileTime) {
                header('HTTP/1.1 304 Not Modified');
                exit;
            }
        }
    }

    // Fetch from API if cache is not valid or outdated
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $city);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $response = curl_exec($ch);
    curl_close($ch);

    if ($response !== false) {
        file_put_contents($cacheFile, $response);
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        return $response;
    }
    //else: error
    return false;

}

// Usage
$apiUrl = 'https://api.example.com/data'; // Replace with the actual API endpoint
$response = getApiResponse($apiUrl);
if(!$response){
    //getting data from nextbike not succeeded
    header('Location: '.$apiUrl);
    die();
}
header('Content-Type: application/json');
echo $response;
?>
