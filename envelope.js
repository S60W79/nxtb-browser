class Envelope{
	constructor(name){
		this.name = name;
		this.url = 'https://bikes.dvb.solutions/envelopes/'+name+'.php';
		this.data = [];
		//find out how old the cached data (if existing) is
		this.state = 0
		if(this.name in localStorage)this.state = JSON.parse(localStorage.getItem(this.name))["server_time"]
	}
loadFromCache(){
	//read out cache and load data
	if(this.name in localStorage)this.data = JSON.parse(localStorage.getItem(this.name))["items"]
}
update(){
	//update bike Types cache if server finds out that file changed:
	updateEnv(this.state, this.url).then(response => {
		//bike types changed! update cache
		//finds out by if-mofified-since header
		this.data = response["items"];
		localStorage.setItem(this.name, JSON.stringify(response));
	}).catch(error => {
	//unchanged or errors: try to load from cache
		this.loadFromCache()
	});
}
updateIfEmpty(){
	if(this.data.length == 0){
		this.update()
		return true
	}else return false;
}
}
