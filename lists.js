class Elements{
    constructor(){
    }

}
class Trans{
    constructor(datum){
        this.node = datum["node"]
        this.date = new Date(parseInt(datum["date"]*1000))
        this.amount = datum["amount"]
        this.provider = datum["provider"]
        this.text = datum["text"]
        this.type = datum["transaction_type"]
        this.symbolic = ""
        switch(this.node){
            case "payment":
                this.symbolic = "🪙";
            break;
            case "transaction":
                this.symbolic = "↔️";
            break;
            case "voucher":
                this.symbolic = "📜"
            break;
            default:
                this.symbolic = "?";
            break;

        }
    }
    html(){
        return `<div class="list-entry"><div class="entry-label">${this.symbolic}</div><div class="entry-label">${this.date.toDateString()}<br>&#150;${this.date.toLocaleTimeString()}</div>${(this.amount!=0&&this.amount!=null)?`<div class="entry-label">${this.amount/100}💵</div>`:""}${(this.provider !=null)?"<div class='entry-label'>"+this.provider+"</div>":''}<div class="entry-label">${this.text}</div>${(this.type!=null)?`<div class="entry-label">${this.type}</div>`:""}</div>`
    }
}
class HistoryElement{
    //entry of one bike ride
    constructor(rental) {
        let start = "Start";
        if (rental["start_place_type"] != 12) start += ":<br>" + rental["start_place_name"];
        let end = "End";
        if (rental["end_place_type"] != 12) end += ":<br>" + rental["end_place_name"];

        this.id = rental["id"];
        this.bike = rental["bike"];
        this.start = rental["start_time"];
        this.end = rental["end_time"]
        this.duration = rental["end_time"] - rental["start_time"];
        this.distance = rental["distance"];
        this.free = rental["used_free_seconds"];
        this.price = rental["price"];
        this.end_id = rental["end_place"];
        this.start_lat = rental["start_place_lat"];
        this.start_lng = rental["start_place_lng"];
        this.start_name = start;
        this.end_lat = rental["end_place_lat"];
        this.end_lng = rental["end_place_lng"];
        this.end_name = end;
        this.stars = rental["rating"];
        this.type = rental["trip_type"];

    }
    html() {
        //generate one line of the history table
        this.hDur = Math.floor(this.duration/3600);
        this.mDur = Math.floor((this.duration%3600)/60);
        this.sDur = Math.floor(this.duration%60);
        if(this.mDur < 10) this.mDur = "0" + this.mDur;
        if(this.sDur < 10) this.sDur = "0" + this.sDur;
        let date = new Date(this.start*1000);
        let end = new Date((this.start + this.duration)*1000);

        this.hStart = date.getHours();
        this.mStart = date.getMinutes();
        this.sStart = date.getSeconds();
        let modifyButton = `<button onclick='showModifier(${this.id},${this.stars},${this.end_id},"${this.end_name}",${this.end_lat},${this.end_lng},${this.type})' class="standBut">✏️</button>`;
        return `<div class="list-entry"><div class="entry-label"><a href="nxtb.html#${authkey}#${this.bike}" target="_blank">${this.bike}</a>${modifyButton}</div><div class="entry-label">${date.toDateString()}<br>${date.toLocaleTimeString()} (&#150;${end.toLocaleTimeString()})</div><div class="entry-label">Duration:&shy;${this.hDur}:${this.mDur}:${this.sDur}<br>Distance:&shy;${this.distance}</div><div class="entry-label">Price:&shy;${Math.floor(this.price/100)}<br>Free&shy;minutes:&shy;${Math.ceil(this.free/60)}</div><button onclick="drawLine(${this.start_lat},${this.start_lng},${this.end_lat},${this.end_lng}, [${this.start}, ${this.end}, ${parseInt(this.bike)}])">${this.start_name} TO ${this.end_name}</button></div>`;

    }
}
class HistoryList{
    constructor(data){
        //reverste makes the sorting last-recent-first
        data = data["account"]["items"].reverse()
        this.lst = []
        console.log("OK", this.lst)
        data.forEach(function(datum){
            if(datum["node"]=="booking")return;
            if(datum["node"]=="rental"){
                this.lst.push(new HistoryElement(datum));
            }else{
                this.lst.push(new Trans(datum));
            }
        }.bind(this))
        console.log("LENGTH", this.lst.length)
    }
    html(){
        let htmlTxt ="";
        this.lst.forEach(function(element){
            htmlTxt+=element.html()
        })
        return htmlTxt
    }
}
