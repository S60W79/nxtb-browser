<?php
//time window
include("../../credentials.php");
$since = 0;
$to = time();
if(isset($_GET["since"]) && $_GET["since"]!= "" && $_GET["since"] != "NaN")$since = $_GET["since"];
if(isset($_GET["to"]) && $_GET["to"]!= "" && $_GET["to"] != "NaN")$to = $_GET["to"];
$onlyStationsStart = False;
if(isset($_GET["onlystationsstart"]) && $_GET["onlystationsstart"]!= "" && $_GET["onlystationsstart"] != "NaN" && $_GET["onlystationsstart"]=="true")$onlyStationsStart = True;
$onlyFieldStart = False;
if(isset($_GET["onlyfieldstart"]) && $_GET["onlyfieldstart"]!= "" && $_GET["onlyfieldstart"] != "NaN" && $_GET["onlyfieldstart"] == "true")$onlyFieldStart = True;
$onlyStationsEnd = False;
if(isset($_GET["onlystationsend"]) && $_GET["onlystationsend"]!= "" && $_GET["onlystationsend"] != "NaN" && $_GET["onlystationsend"] == "true")$onlyStationsEnd = True;
$onlyFieldEnd = False;
if(isset($_GET["onlyfieldend"]) && $_GET["onlyfieldend"]!= "" && $_GET["onlyfieldend"] != "NaN" && $_GET["onlyfieldend"]=="true")$onlyFieldEnd = True;
$returnal = array();

$start = False;
$end = False;
$bike = False;
if(isset($_GET["start"]) && $_GET["start"]!= "" && $_GET["start"] != "NaN")$start = $_GET["start"]; //start(station(s)
if(isset($_GET["end"]) && $_GET["end"]!= "" && $_GET["end"] != "NaN")$end = $_GET["end"];//end station(s)
if(isset($_GET["bike"]) && $_GET["bike"]!= "" && $_GET["bike"] != "NaN")$bike = $_GET["bike"];//end station(s)
$endLatMin = -1000;
$endLatMax = 1000;
$endLngMin = -1000;
$endLngMax = 1000;
if(isset($_GET["endLatMin"]) && $_GET["endLatMin"]!= "" && $_GET["endLatMin"] != "NaN")$endLatMin = $_GET["endLatMin"];
if(isset($_GET["endLatMax"]) && $_GET["endLatMax"]!= "" && $_GET["endLatMax"] != "NaN")$endLatMax = $_GET["endLatMax"];
if(isset($_GET["endLngMin"]) && $_GET["endLngMin"]!= "" && $_GET["endLngMin"] != "NaN")$endLngMin = $_GET["endLngMin"];
if(isset($_GET["endLngMax"]) && $_GET["endLngMax"]!= "" && $_GET["endLngMax"] != "NaN")$endLngMax = $_GET["endLngMax"];
$ex=array(intval($since), intval($to), floatval($endLatMin), floatval($endLatMax), floatval($endLngMin), floatval($endLngMax));
$pdo = new PDO('mysql:host=localhost;dbname=bikes', $username, $pass);
$statement = $pdo->prepare("SELECT * FROM drives WHERE start_time>? AND end_time<? AND end_lat > ? AND end_lat < ? AND end_lng > ? AND end_lng < ?");
$statement->execute($ex);  
$i = 0;
while($row = $statement->fetch()) {
   if($onlyStationsStart && $row["start_id"] == 0)continue;
   if($onlyFieldStart && $row["start_id"] != 0)continue;
   if($onlyStationsEnd && $row["end_id"] == 0)continue;
   if($onlyFieldEnd&& $row["end_id"] != 0)continue;
   if($start && !in_array($row["start_id"], explode(",", $start)))continue;
   if($end && !in_array($row["end_id"], explode(",", $end)))continue;
   if($bike && !in_array($row["bike"], explode(",", $bike)))continue;
    array_push($returnal, $row);
    $i++;
    if($i==100000){
      array_push($returnal, "break");
       break;
    }
}
echo json_encode($returnal);
?>
