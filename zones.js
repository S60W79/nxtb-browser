 async function getZones(domain, hash){
    //get a geojson in which the service zone(s) are depicted
    return new Promise((resolve, reject) => {

        var xhr = new XMLHttpRequest();
        xhr.open("POST", "https://api.nextbike.net/api/getFlexzones.json");
        // xhr.setRequestHeader("Accept", "application/json");
        // xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onload = function () {
            if (xhr.status == 200) {
                console.log(xhr.status);
                var plain =  xhr.responseText;

                if (plain != null && plain != ""){
                    var sheet = JSON.parse(plain)["geojson"];
                    resolve(sheet);
                }
            }else{
            console.log("error", xhr.status);
            console.log("error", xhr.responseText);
            reject(xhr.status);
            }

        };

        var data = `{
        "api_key": "`+apikey+`",
        "include_internal": true,
        "domain": "`+domain+`",
        "hash": "`+hash+`",
        "show_errors":1
        }`;
        xhr.send(data);
    });
}

class legendEntry{

    constructor(geojson){
        this.color = '';
        this.name='';
        this.category=''
        this.area=[];
        this.intern=false;
        this.importArea(geojson);
    }
    importArea(geojson){
        this.color=geojson["properties"]["color"]
        this.name=geojson["properties"]["name"]
        this.category=geojson["properties"]["category"]
        this.area=geojson["geometry"]["coordinates"]
        this.intern = (geojson["properties"]["public"] == "0")
    }
    exportHtml(inclName, inclIntern){
        if(!inclIntern && this.intern)return "";
        return "<div style='color:white; background-color:"+this.color+";'>"+this.category.replace("_"," ")+(inclName?("<br>"+this.name):"")+"</div>";
    }
}
